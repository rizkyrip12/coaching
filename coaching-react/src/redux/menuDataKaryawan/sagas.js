import { all, takeEvery, put, fork, select, call } from 'redux-saga/effects'
import * as types from '../../constants/ActionTypes'
import { setLoader } from './action'
import { MessageError } from '../../components/messageBox'
import { GET, POST, PUT, DELETE } from '../../constants/API'



const getMenuDataKaryawan = (state) => state.MenuDataKaryawanReducer

export function* getDataKaryawan(action) {
    try {
        yield put(setLoader(true))
        if (action.value === "index") {
            const _response = yield call(GET, 'https://restapirizky.herokuapp.com/api/dataKaryawan')
            if (_response.status === 200) {
                let _data = []
                _response.response.rows.map((x, i) => {
                    _data.push({
                        'nik': x.nik,
                        'nama': x.nama,
                        'telfon': x.telfon,
                        'email': x.email
                    })
                })
                yield put({ type: types.HANDLE_STATE_OUTER, field: 'dataGridForm', value: _data })
                yield put(setLoader(false))
            } else {
                MessageError()
                yield put(setLoader(false))
                return
            }
        }
    } catch (error) {
        MessageError(error)
    }
}

export function* submitDataKaryawan(action) {
    const state = yield select(getMenuDataKaryawan)
    try {
        yield put(setLoader(true))
        if (action.value === "submit") {
            let _body = {
                "name": state.form.name,
                "telpon": state.form.telfon,
                "email": state.form.email
            }
            const _response = yield call(POST, "https://restapirizky.herokuapp.com/api/addDataKaryawan", _body)
            window.history.back()
        }
    } catch (error) {
        MessageError(error)
    }
}

export function* deleteDataKaryawan(action) {
    const state = yield select(getMenuDataKaryawan)
    try {
        yield put(setLoader(true))
        let _body = {
            nik: action.value
        }
        const _response = yield call(DELETE, "https://restapirizky.herokuapp.com/api/removeDataKaryawan", _body)
        if (_response.status == 200) {
            const _responseGetData = yield call(GET, 'https://restapirizky.herokuapp.com/api/dataKaryawan')
            if (_responseGetData.status === 200) {
                let _data = []
                _responseGetData.response.rows.map((x, i) => {
                    _data.push({
                        'nik': x.nik,
                        'nama': x.nama,
                        'telfon': x.telfon,
                        'email': x.email
                    })
                })
                yield put({ type: types.HANDLE_STATE_OUTER, field: 'dataGridForm', value: _data })
                yield put(setLoader(false))
            } else {
                MessageError()
                yield put(setLoader(false))
                return
            }
        }
    } catch (error) {
        MessageError(error)
    }
}

export function* getDetailData(action) {
    try {
        yield put(setLoader(true))
        const _response = yield call(GET, 'https://restapirizky.herokuapp.com/api/dataKaryawan')
        if (_response.status === 200) {
            let _data = []
            let _form = []
            _response.response.rows.filter(obj => obj.nik == action.value).map((x, i) => {
                _data.push({
                    'nik': x.nik,
                    'nama': x.nama,
                    'telfon': x.telfon,
                    'email': x.email
                })
            })
            _form = {
                name: _data[0].nama,
                telfon: _data[0].telfon,
                email: _data[0].email
            }
            console.log('cek ', _data)
            yield put({ type: types.HANDLE_STATE_OUTER, field: 'form', value: _form })
            yield put(setLoader(false))
        } else {
            yield put(setLoader(false))
            MessageError()
            return
        }
    } catch (error) {
        MessageError(error)
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(types.GET_DATA_INIT, getDataKaryawan),
        takeEvery(types.HANDLE_SUBMIT, submitDataKaryawan),
        takeEvery(types.HANDLE_DELETE, deleteDataKaryawan),
        takeEvery(types.GET_DETAIL_DATA, getDetailData)
    ])

}
import {
    INIT_STATE,
    HANDLE_STATE_MENU_DATA_KARYAWAN,
    HANDLE_STATE_OUTER,
    SET_LOADER
} from '../../constants/ActionTypes'

const initState = {
    isCreate: false,
    dataGridForm: [],
    isLoader: false,
    form: {
        name: undefined,
        telfon: undefined,
        email: undefined
    }
}

const menuDataKaryawan = (state = initState, action) => {
    switch (action.type) {
        case SET_LOADER: {
            return {
                ...state, isLoader: action.value
            }
        }
        case INIT_STATE: {
            return {
                ...state,
                dataGridForm: [],
                isCreate: false,
                isLoader: false,
                form: {
                    name: undefined,
                    telfon: undefined,
                    email: undefined
                }
            }
        }
        case HANDLE_STATE_OUTER: {
            return {
                ...state, [action.field]: action.value
            }
        }
        case HANDLE_STATE_MENU_DATA_KARYAWAN: {
            return {
                ...state, form: { ...state.form, [action.field]: action.value}
            }
        }
        default:
            return state;
    }
}

export default menuDataKaryawan
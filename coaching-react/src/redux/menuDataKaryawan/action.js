import * as types from '../../constants/ActionTypes'


export const getDataInit = (value) => {
    return ({
        type: types.GET_DATA_INIT,
        value
    })
}

export const initState = () => {
    return ({
        type: types.INIT_STATE,
    })
}

export const handleStateOuter = (field, value) => {
    return ({
        type: types.HANDLE_STATE_OUTER,
        field,
        value
    })
}

export const handleState = (field, value) => {
    return ({
        type: types.HANDLE_STATE_MENU_DATA_KARYAWAN,
        field,
        value
    })
}

export const handleSubmit = (value) => {
    return ({
        type: types.HANDLE_SUBMIT,
        value
    })
}

export const handleDelete = (value) => {
    return ({
        type: types.HANDLE_DELETE,
        value
    })
}

export const getDetailData = (value) => {
    return ({
        type: types.GET_DETAIL_DATA,
        value
    })
}

export const setLoader = (value) => {
    return ({
        type: types.SET_LOADER,
        value
    })
}


export const action = {
    getDataInit,
    initState,
    handleStateOuter,
    handleState,
    handleSubmit,
    handleDelete,
    getDetailData,
    setLoader
}
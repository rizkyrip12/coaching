import { all } from 'redux-saga/effects'
import menuDataKaryawanSagas from './menuDataKaryawan/sagas'

export default function* rootSaga(getState) {
    yield all([
      menuDataKaryawanSagas()
    ]);
  }

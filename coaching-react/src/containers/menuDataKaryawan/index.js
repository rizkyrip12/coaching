import React, { Component } from "react";
import { connect } from "react-redux";
import { Table, Input, Button, Spin, Alert } from 'antd';
import { Link } from "react-router-dom";
import { getDataInit, initState, handleStateOuter, handleDelete, getDetailData, setLoader } from '../../redux/menuDataKaryawan/action'

// let columns
const ButtonGroup = Button.Group;

class dataKaryawan extends Component {
  constructor(props) {
    super()
    this.state = {
      columns: [
        {
          title: 'NIK',
          dataIndex: 'nik',
          key: 'nik',
          width: '30%',
          ...this.getColumnSearchProps('nik'),
        },
        {
          title: 'Name',
          dataIndex: 'nama',
          key: 'nama',
          width: '30%',
          ...this.getColumnSearchProps('nama'),
        },
        {
          title: 'Telfon',
          dataIndex: 'telfon',
          key: 'telfon',
          width: '20%',
          ...this.getColumnSearchProps('telfon'),
        },
        {
          title: 'Email',
          dataIndex: 'email',
          key: 'email',
          ...this.getColumnSearchProps('email'),
        },
        {
          title: 'Action',
          key: 'action',
          render: (text, record) => (
            <span>
              {/* <Button onClick={() => {
                this.getDetail(record.nik)
                this.getDataInit()
              }}>
                <Link to={`/add-data-karyawan/${record.nik}`}>
                  <a>Edit</a>
                </Link>
              </Button> */}
              <Button onClick={() => {
                this.deleteData(record.nik)
              }}>
                <a>Delete</a>
              </Button>
            </span>
          ),
        },
      ],
    }
  }
  state = {
    searchText: '',
    searchedColumn: '',
  };

  getDataInit = () => {
    let { getDataInit } = this.props

    getDataInit()
  }

  handleStateOuter = (field, value) => {
    let { handleStateOuter } = this.props

    handleStateOuter(field, value)
  }

  handleLoader = () => {
    let { setLoader } = this.props

    setLoader(false)
  }

  getDetail = (value) => {
    let { getDetailData } = this.props

    getDetailData(value)
  }

  deleteData = (value) => {
    let { handleDelete } = this.props

    handleDelete(value)
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Seacrh
        </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
        </Button>
      </div>
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        text.toString()
      ) : (
        text
      )
  });

  componentWillMount() {
    let { initState, getDataInit } = this.props

    initState()
    getDataInit("index")
  }

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  render() {
    let { menuDataKaryawan } = this.props
    return (
      <Spin spinning={menuDataKaryawan.isLoader}>
        <Button
          title="Add"
          className="addBtn"
          style={{ marginTop: "50px" }}
          onClick={() => {
            this.handleStateOuter("isCreate", true)}}
        >
          <Link to={`/add-data-karyawan`}>
            Add Data
          </Link>
        </Button>
        <Table style={{ marginTop: '50px' }} columns={this.state.columns} dataSource={menuDataKaryawan.dataGridForm} />
      </Spin>
    )
  }
}

const mapStateToProps = (state) => ({
  menuDataKaryawan: state.MenuDataKaryawanReducer,
  appState: state.App
})
export default connect(
  mapStateToProps, { getDataInit, initState, handleStateOuter, handleDelete, getDetailData, setLoader }
)(dataKaryawan)

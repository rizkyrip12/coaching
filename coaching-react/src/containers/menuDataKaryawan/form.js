import React, { Component } from "react";
import { connect } from "react-redux";
import { Input, Button, Form, Spin, Alert } from 'antd';
import { Link } from "react-router-dom"
import { MessgaeValidasi } from '../../components/messageBox'
import { getDataInit, initState, handleState, handleSubmit } from '../../redux/menuDataKaryawan/action'


class formKaryawan extends Component {
  constructor(props) {
    super()
    this.state = {
    }
  }
  state = {
    searchText: '',
    searchedColumn: '',
  };

  handleSubmit = (value) => {
    let { menuDataKaryawan, handleSubmit } = this.props
    if (value) {
      if (menuDataKaryawan.form.name == undefined || menuDataKaryawan.form.name == "") {
        MessgaeValidasi("Nama Tidak Boleh Kosong")
        return
      }
      if (menuDataKaryawan.form.telfon == undefined || menuDataKaryawan.form.telfon == "") {
        MessgaeValidasi("Telfon Tidak Boleh Kosong")
        return
      }
      if (menuDataKaryawan.form.email == undefined || menuDataKaryawan.form.email == "") {
        MessgaeValidasi("Email Tidak Boleh Kosong")
        return
      }
      console.log('1')
      handleSubmit("submit")
    }

  };

  handleStateForm = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }

  handleStateForm = (field, value) => {
    let { handleState } = this.props

    handleState(field, value)
  }


  componentWillMount() {
    // let { getDataInit } = this.props
    // getDataInit()
  }

  componentDidMount() {
    let { getDataInit } = this.props
    getDataInit()
  }

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  render() {
    const { menuDataKaryawan } = this.props
    const { getFieldDecorator } = this.props.form
    const layout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
    };

    return (
      <Spin spinning={ menuDataKaryawan.isCreate ? false : menuDataKaryawan.isLoader}>
        <Form {...layout} name="nest-messages" style={{ marginTop: "50px" }}>
          {
            console.log('cek isi data ', menuDataKaryawan.form.name)
          }
          <Form.Item label="Name" initialValues={menuDataKaryawan.form.name} onSubmit={this.handleSubmitForm}>
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Please input your name!' }],
            })(<Input
              placeholder="Name"
              id="name"
              name="name"
              initialValue={menuDataKaryawan.form.name}
              onChange={((e) => this.handleStateForm("name", e.target.value))}
            />)}
          </Form.Item>
          <Form.Item name={['telfon']} label="Telfon" initialValue={menuDataKaryawan.form.telfon} rules={[{ required: true }]}>
            {getFieldDecorator('telfon', {
              rules: [{ required: true, message: 'Please input your name!' }],
            })(<Input
              placeholder="Telfon"
              id="telfon"
              onChange={((e) => this.handleStateForm("telfon", e.target.value))}
            />)}
          </Form.Item>
          <Form.Item name={['email']} label="Email" initialValue={menuDataKaryawan.form.email} rules={[{ type: 'email', required: true }]}>
            {getFieldDecorator('email', {
              rules: [{ required: true, message: 'Please input your name!' }],
            })(<Input
              placeholder="Email"
              id="email"
              onChange={((e) => this.handleStateForm("email", e.target.value))}
            />)}
          </Form.Item>
          <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
            <Button type="primary" htmlType="submit" onClick={() => {
              this.handleSubmit(menuDataKaryawan.isCreate)
            }}>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Spin>
    );
  }
}

const mapStateToProps = (state) => ({
  menuDataKaryawan: state.MenuDataKaryawanReducer,
  appState: state.App
})
export default connect(
  mapStateToProps, { getDataInit, initState, handleState, handleSubmit }
)(Form.create()(formKaryawan))

import React from 'react';
import 'react-pro-sidebar/dist/css/styles.css';
import './App.css';
import { Provider } from 'react-redux';
import { store, history } from '../../redux/store';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Layout from '../../navbar'
import Home from '../menuHome'
import Data from '../menuDataKaryawan'
import Detail from '../menuDataKaryawan/form'
import { Container } from 'react-bootstrap';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loginStatus: false
    }
  }

  handleChangeStatus = (value) => {
    this.setState({
      loginStatus: value
    })
  }

  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <Layout />
          <Container>
            <Switch>
              <Route exact path='/' component={Home} />
              <Route path='/dataKaryawan' component={Data} />
              <Route path='/add-data-karyawan' component={Detail}/>
              <Route path='/add-data-karyawan/:id' component={Detail}/>
            </Switch>
          </Container>
        </Router>
      </Provider>

    );
  }

}

export default App;

import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'antd'
import {
    Container,
    Navbar,
} from 'react-bootstrap';
import styled from 'styled-components';


const Styles = styled.div`
    .navbar{
        background-color: grey;
    }

    a, .navbar-brand, .navbar-nav .nav-link {
        color: blue;
        &:hover{
            color:white;
        }
    }
    `;

class Layout extends React.Component {
    
    render() {
        return (
            <Styles>
                <Navbar expand="lg" >
                    <Container>
                        <Navbar.Brand>
                            <Button className=" btn btn-warning" style={{marginRight: '5px'}}><Link to="/">Home</Link></Button>
                            <Button className=" btn btn-success"><Link to="/dataKaryawan">List Data</Link></Button>
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    </Container>
                </Navbar>
            </Styles>
        )
    }

}

export default Layout;
import axios from 'axios';
import Swal from 'sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css'



export function POST(url, body, type) {
    return axios({
        method: "post",
        url: url,
        data: body
    })
        .then((res) => {
            if (res.status == 200 || res.status == 201) {
                let _data = res.data
                if (_data.length == 1) {
                    if (_data[0].acknowledge == 0) {
                        if (type != undefined) {
                            Swal.fire({
                                title: 'Error',
                                text: _data[0].message,
                                type: 'error',
                                allowOutsideClick: false,
                                confirmButtonText: 'Ya',
                            }).then(function (confirm) {
                            })
                        }
                    }
                }

                return res.data
            }
        })
        .catch((err) => {
            if (err.response != undefined) {
                return {
                    status: 500,
                    acknowledge: 0
                }
            }
        })
}


export function GET(url) {
    return axios({
        method: "get",
        url: url
    })
        .then((res) => {
            if (res.status == 200) {
                let _data = res.data

                if (Array.isArray(_data)) {
                    if (_data[0].acknowledge == 0) {
                        Swal.fire({
                            title: 'Error',
                            text: _data[0].message,
                            type: 'error',
                            allowOutsideClick: false,
                            confirmButtonText: 'Ya',
                        }).then(function (confirm) {
                        })
                    }
                }
                else {
                    if (_data.acknowledge == 0) {
                        Swal.fire({
                            title: 'Error',
                            text: _data.message,
                            type: 'error',
                            allowOutsideClick: false,
                            confirmButtonText: 'Ya',
                        }).then(function (confirm) {
                        })
                    }
                }

                return res.data
            }
        })
        .catch((err) => {
            return err
        })
}


export function PUT(url, body) {
    return axios.put(url, body)
        .then((res) => {
            if (res.status == 200) {
                let _data = res.data

                if (Array.isArray(_data)) {
                    if (_data[0].acknowledge == 0) {
                        Swal.fire({
                            title: 'Error',
                            text: _data[0].message,
                            type: 'error',
                            allowOutsideClick: false,
                            confirmButtonText: 'Ya',
                        }).then(function (confirm) {
                        })
                    }
                }
                else {
                    if (_data.acknowledge == 0) {
                        Swal.fire({
                            title: 'Error',
                            text: _data.message,
                            type: 'error',
                            allowOutsideClick: false,
                            confirmButtonText: 'Ya',
                        }).then(function (confirm) {
                        })
                    }
                }

                return res.data
            }
        })
        .catch((err) => {
            return err
        })
}

export function DELETE(url, body, type) {
    return axios({
        method: "delete",
        url: url,
        data: body
    })
        .then((res) => {
            if (res.status == 200 || res.status == 201) {
                let _data = res.data
                if (_data.length == 1) {
                    if (_data[0].acknowledge == 0) {
                        if (type != undefined) {
                            Swal.fire({
                                title: 'Error',
                                text: _data[0].message,
                                type: 'error',
                                allowOutsideClick: false,
                                confirmButtonText: 'Ya',
                            }).then(function (confirm) {
                            })
                        }
                    }
                }

                return res.data
            }
        })
        .catch((err) => {
            if (err.response != undefined) {
                return {
                    status: 500,
                    acknowledge: 0
                }
            }
        })
}
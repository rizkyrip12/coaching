import Swal from 'sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css'

import React from "react";
import { notification, Icon } from 'antd';

export function MessageSuccess(data) {
    Swal.fire({
        title: 'success!',
        text: data,
        type: 'success',
    })
}

export function MessageError(data) {
    Swal.fire({
        title: 'Oops! Data tidak ditemukan',
        text: data,
        type: 'error',
    })
}

export function MessgaeValidasi(data) {
    Swal.fire({
        title: 'Oops! ',
        text: data,
        type: 'error',
    })
}

export function MessageInfo(data) {
    Swal.fire({
        title: 'Oops!',
        text: data,
        type: 'info',
    })
}